from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import os

import json
import logging
from qmri_core.utilities import configuration_utilities



class Configuration(object):
    def __init__(self, configObject):
        self.configObject = configObject
        self.required = False

    def parse_configuration(self):       
        group_task = self.configObject.parser.add_argument_group('method_config')
        group_task.add_argument('-inputChannels', required=self.required, type=int, help='Number of input channels')
        group_task.add_argument('-outputChannelsLayer1', required=self.required, type=int, help='Number of output channels in layer 1')
        group_task.add_argument('-outputChannelsLayer2', required=self.required, type=int, help='Number of output channels in layer 2')
        group_task.add_argument('-outputChannelsLayer3', required=self.required, type=int, help='Number of output channels in layer 3')
        group_task.add_argument('-outputChannelsLayer4', required=self.required, type=int, help='Number of output channels in layer 4')
        group_task.add_argument('-outputChannelsLayer5', required=self.required, type=int, help='Number of output channels in layer 5')
        group_task.add_argument('-outputChannelsLayer6', required=self.required, type=int, help='Number of output channels in layer 6')
        group_task.add_argument('-outputChannelsLayer7', required=self.required, type=int, help='Number of output channels in layer 7')
        group_task.add_argument('-outputChannels', required=self.required, type=int, help='Number of output channels')
        group_task.add_argument('-convLayersInResidualBlock', required=self.required, type=int, help='Number of convolutional layers per residual block')
        group_task.add_argument('-useMeanBatchNorm', required=self.required, type=int, help='If True, first batch normalisation layer will subtract the mean from the data')

        config_args, _ = self.configObject.parser.parse_known_args()
        configuration = configuration_utilities.convert_argparse_to_attr(config_args)
        return configuration


# class ResNetConfiguration(object):
#     def __init__(self, globalConfig):
#         self.globalConfig = globalConfig
#         if os.path.isfile(self.globalConfig.args.inferenceModelConfigurationFile):
#             configuration = configuration_utilities.parse_json_configuration(self.globalConfig.args.inferenceModelConfigurationFile)
#             self.globalConfig.args.__update__(**configuration)
#             self.required = False
#         else:
#             self.required = True

#         ## Optional if configuration file is present
#         cmd_configuration = self.parse_command_line_configuration()
#         self.globalConfig.args.__update__(**cmd_configuration)

#     def parse_command_line_configuration(self):

#         group_task = self.globalConfig.parser.add_argument_group('method_config')
#         group_task.add_argument('-inputChannels', required=self.required, type=int, help='Number of input channels')
#         group_task.add_argument('-outputChannelsLayer1', required=self.required, type=int, help='Number of output channels in layer 1')
#         group_task.add_argument('-outputChannelsLayer2', required=self.required, type=int, help='Number of output channels in layer 2')
#         group_task.add_argument('-outputChannelsLayer3', required=self.required, type=int, help='Number of output channels in layer 3')
#         group_task.add_argument('-outputChannelsLayer4', required=self.required, type=int, help='Number of output channels in layer 4')
#         group_task.add_argument('-outputChannelsLayer5', required=self.required, type=int, help='Number of output channels in layer 5')
#         group_task.add_argument('-outputChannelsLayer6', required=self.required, type=int, help='Number of output channels in layer 6')
#         group_task.add_argument('-outputChannelsLayer7', required=self.required, type=int, help='Number of output channels in layer 7')
#         group_task.add_argument('-outputChannels', required=self.required, type=int, help='Number of output channels')
#         group_task.add_argument('-convLayersInResidualBlock', required=self.required, type=int, help='Number of convolutional layers per residual block')
#         group_task.add_argument('-useMeanBatchNorm', required=self.required, type=int, help='If True, first batch normalisation layer will subtract the mean from the data')

#         config_args, _ = self.globalConfig.parser.parse_known_args()
#         configuration = configuration_utilities.convert_argparse_to_attr(config_args)
#         return configuration
