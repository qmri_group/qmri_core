from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

import logging
import numpy as np
from qmri_core.utilities import image_utilities
from qmri_core.utilities import core_utilities
from qmri_core.utilities import checkpoint_utilities as checkpoint

logging.basicConfig(level=logging.INFO)
logging.info('Setting up environment...')


def train(configObject):
    """Performs a forward pass through a given inference model.
        Depending on the options set, it might save intermediate results and training checkpoints
        and/or display the intermediate results.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - epochs
            - inference_model
            - dataloader
            - signal_model (if configObject.args.inference_model.__require_initial_guess__ == True)
            - numberOfPatches
            - objective_fun
            - optimizer
            - saveResults
            - saveResultsPath
            - saveCheckpoint
            - saveCheckpointPath
    """
    configObject.args.state = core_utilities.alternateTrainingState(configObject.args)
    epoch = 0
    for _ in range(configObject.args.epochs):
        state = next(configObject.args.state)
        dataloader = list(state.values())[0]
        configObject.args.stateName = list(state.keys())[0]
        configObject.args.inference_model.train() if configObject.args.stateName == 'training' else configObject.args.inference_model.eval()

        if configObject.args.stateName == 'training': 
            epoch += 1

        logging.info('Running {}'.format(configObject.args.stateName))
        logging.info('*'*50)

        configObject.args.lenDataset = len(dataloader)
        for i, data in enumerate(dataloader):
            configObject.args.iter = i
            #TODO: if configObject.args.usePatches: #Because if we use patches, we can divide them into batches. Otherwise the dataloader deals with batch size.

            batch_generator = core_utilities.batch_iterator(configObject.args, state['batchSize'])
            patch_iterations = int(np.ceil(configObject.args.numberOfPatches/state['batchSize']))
            for _ in range(patch_iterations):
                next(batch_generator)
                batched_data, indexes = core_utilities.get_batch(data, configObject.args, batch_generator)
                signal = batched_data[0]
                if len(batched_data) > 1:
                    label = batched_data[1]
                    mask = batched_data[2]

                if configObject.args.inference_model.__require_initial_guess__:
                    initial_kappa = configObject.args.signal_model.initializeParameters(signal, configObject.args)
                    inputs = [signal, initial_kappa]
                else:
                    inputs = [signal]

                estimate = configObject.args.inference_model.forward(inputs)

                if isinstance(estimate, list):
                    loss = [configObject.args.objective_fun(e*mask, label*mask) for e in estimate]
                    loss = sum(loss) / len(loss)
                    last_estimate = estimate[-1].cpu().detach().numpy()
                else:
                    loss = configObject.args.objective_fun(estimate*mask, label*mask)
                    last_estimate = estimate.cpu().detach().numpy()

                if configObject.args.stateName == 'training':
                    loss.backward()
                    configObject.args.optimizer.step()
                    configObject.args.optimizer.zero_grad()

                logging.info("Epoch: {}, State: {}, Subject: {}/{}, Patches {}/{}, Loss: {} ".format(epoch, 
                                    configObject.args.stateName, i, dataloader.__len__(), 
                                    indexes[0], configObject.args.numberOfPatches, loss)
                            )

        if configObject.args.saveResults:
            if not configObject.args.saveResultsPath:
                logging.warning('Please specify path to save intermediary results - saveResultsPath')
                break
            else:
                logging.warning("SAVING DATA")
                data_ = {}
                data_['estimated'] = last_estimate
                if len(batched_data) > 1:
                    data_['labels'] = label[0].cpu().detach().numpy()
                    data_['mask'] = mask[0].cpu().detach().numpy()
                image_utilities.saveItermediateResults(data_, configObject.args, epoch)

        if configObject.args.stateName == 'training' and configObject.args.saveCheckpoint:
            checkpoint.save(configObject.args, epoch, configObject.args.inference_model)
