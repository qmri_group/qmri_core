from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import os

import torch
from torch.utils.data import DataLoader
from torch.utils.data import Dataset


class SampleDatabase(Dataset):
    """ Wrapper function for custom dataset subclasses.
    """
    def __init__(self, dataset, path):
        self.dataset = dataset
        self.dataset.set_data_path(path)
        self.dataset.index_files()

    def __len__(self):
        return self.dataset.get_length()

    def __getitem__(self, idx):
        label, mask = self.dataset.get_label(idx)
        filename = self.dataset.fileName[idx][:-5]
        signal = self.dataset.get_signal(idx)

        if len(label)==0:
            return signal, filename
        else:
            return signal, filename, label, mask


def create_dataloader(configObject):
    """Creates the dataloader handler

    Args:
        configObject ([Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - mode
            - dataset
            - trainingDataPath, validationDataPath and/or testingDataPath
            - batchSize
            - usePatches
            - useSimulatedData
            - runValidation
            
    Returns:
        [type: List]: List of DataLoader objects. If mode=='TRAINING', it returns 
                      [trainingDataloader, validationDataloader]. If mode=='TESTING', it returns 
                      [testingDataloader]
    """

    if configObject.args.mode == 'TRAINING':
        training_database_handler = SampleDatabase(args=configObject.args.dataset, path=configObject.args.trainingDataPath)
        trainingDataloader = DataLoader(training_database_handler,
                                batch_size = 1 if configObject.args.usePatches else configObject.args.batchSize,
                                shuffle = False,
                                num_workers = 0
                                )
        if not configObject.args.useSimulatedData and configObject.args.runValidation:
            validation_database_handler = SampleDatabase(args=configObject.args.dataset, path=configObject.args.validationDataPath)
            validationDataloader = DataLoader(validation_database_handler,
                                    batch_size = 1 if configObject.args.usePatches else configObject.args.batchSize,
                                    shuffle = False,
                                    num_workers = 0
                                    )
            dataloader = [trainingDataloader, validationDataloader]
        else:
            dataloader = [trainingDataloader]

    elif configObject.args.mode == 'TESTING':
        testing_database_handler = SampleDatabase(args=configObject.args.dataset, path=configObject.args.testingDataPath)
        testingDataloader = DataLoader(testing_database_handler,
                                batch_size = 1,
                                shuffle = False,
                                num_workers = 0
                                )
        dataloader = [testingDataloader]

    return dataloader