from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
# import checkpoint

import importlib
import logging
from qmri_core.utilities import checkpoint_utilities
from qmri_core.utilities import core_utilities
from qmri_core.engine import data_loader
import torch
import torch.nn as nn



def make(configObject):
    """Links all configurations to internal modules, which are dinamically imported.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - useCUDA
            - device
            - runBenchmark
            - likelihoodModel
            - signalModel
            - inferenceModel
            - task
            - optimizer
            - learningRate
            - lossFunction
            - loadCheckpoint
            - loadCheckpointPath

    Returns:
        [type: Configuration]: Updated configObject containing all imported Python modules
    """

    if configObject.args.useCUDA:
        if torch.cuda.is_available():
            configObject.args.device = torch.device("cuda:0")
            logging.info("Using CUDA")
        else:
            logging.warn("User selected runtime with CUDA, but CUDA is not available. Running on CPU instead.")
            configObject.args.device = torch.device("cpu")
    else:
        configObject.args.device = torch.device("cpu")
        logging.info("Using CPU")
    torch.backends.cudnn.benchmark = configObject.args.runBenchmark


    ###############################################################
    ###############################################################
    # VALIDATED LIKELIHOOD MODELS
    # OPTIONS: {'gaussian', 'rician'}
    if not hasattr(configObject.args, 'likelihood_model'):
        module_name = configObject.args.likelihoodModel
        module = importlib.import_module("qmri_core.modules.likelihood_model." + module_name + "." + module_name)
        loaded_module = core_utilities.str_to_class(module, module_name.capitalize())
        configObject.args.likelihood_model = loaded_module(configObject)


    ################################################################
    ################################################################
    # VALIDATED SIGNAL MODELS
    # OPTIONS: {'looklocker', 'fse'}
    if not hasattr(configObject.args, 'signal_model'):
        module_name = configObject.args.signalModel
        module = importlib.import_module("qmri_core.modules.signal_model." + module_name + "." + module_name)
        loaded_module = core_utilities.str_to_class(module, module_name.capitalize())
        configObject.args.signal_model = loaded_module(configObject)


    ################################################################
    ################################################################
    # VALIDATED INFERENCE MODELS
    # OPTIONS: {'rim', 'mle', 'resnet'}
    if not hasattr(configObject.args, 'inference_model'):
        module_name = configObject.args.inferenceModel
        module = importlib.import_module("qmri_core.modules.inference_model." + module_name + "." + module_name)
        loaded_module = core_utilities.str_to_class(module, module_name.capitalize())
        configObject.args.inference_model = loaded_module(configObject)


    ################################################################
    ################################################################
    # VALIDATED DATASETS
    # OPTIONS: {'relaxometry'}
    if not hasattr(configObject.args, 'dataset'):
        module_name = configObject.args.task
        module = importlib.import_module("qmri_core.modules.dataset." + module_name + "." + module_name + '_dataset')
        configObject.args.dataset = module.DatasetModel(configObject)


    ################################################################
    ################################################################
    # OPTIMIZER
    if isinstance(configObject.args.inference_model, nn.Module):
        optimizer_options = ['ADAM']
        if configObject.args.optimizer == optimizer_options[0]:
            configObject.args.optimizer = torch.optim.Adam(configObject.args.inference_model.parameters(), lr=configObject.args.learningRate)

    
    ################################################################
    ################################################################
    # OPTIMIZER COST FUNCTION
    costFun_options = ['MSE']
    if configObject.args.lossFunction == costFun_options[0]:
        configObject.args.objective_fun = torch.nn.MSELoss(reduction='mean')
    else:
        logging.warn("Loss function '{}' not supported. Either choose MSE or implement your own loss function".format(configObject.args.lossFunction))


    ################################################################
    ################################################################
    # DEFINITION OF DATALOADER 
    dataloader = data_loader.create_dataloader(configObject)
    configObject.args.dataloader = dataloader

    ################################################################
    ################################################################
    # LOAD PRE-TRAINED MODEL
    if isinstance(configObject.args.inference_model, nn.Module):
        if configObject.args.loadCheckpoint:
            logging.info("Loading model checkpoint from {}.".format(configObject.args.loadCheckpointPath))
            checkpoint_utilities.load(configObject)
       
    return configObject