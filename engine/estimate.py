from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

import logging
import os
from qmri_core.utilities import image_utilities
import torch.nn as nn
import torch

logging.basicConfig(level=logging.INFO)
logging.info('Setting up environment...')

def test(configObject):
    """Performs a forward pass through a given inference model.
        Depending on the options set, it might save and/or display the results.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - inference_model
            - dataloader
            - signal_model (if configObject.args.inference_model.__require_initial_guess__ == True)
            - saveResultsPath
    """

    if isinstance(configObject.args.inference_model, nn.Module):
        configObject.args.inference_model.eval()

    configObject.args.batchSizeIter = 1
    configObject.args.lenDataset = len(configObject.args.dataloader)
    configObject.args.stateName = 'testing'
    for i, data in enumerate(configObject.args.dataloader):
        configObject.args.iter = i
        signal = data[0][0]
        configObject.args.filename = data[1][0]
        if len(data) > 2:
            label = data[2]
        if len(data) > 3:
            mask = data[3]

        if configObject.args.inference_model.__require_initial_guess__:
            initial_kappa = configObject.args.signal_model.initializeParameters(signal, configObject.args)
            inputs = [signal, initial_kappa]
        else:
            inputs = [signal]

        estimate = configObject.args.inference_model.forward(inputs)

        if not os.path.isfile(configObject.args.saveResultsPath):
            logging.error('Please specify a valid path to save intermediary results')
            break
        else:
            data_dict = {}
            data_dict['estimated'] = estimate.detach().numpy()
            if len(data) > 2:
                data_dict['label'] = label.detach().numpy()
            if len(data) > 3:
                data_dict['mask'] = mask.detach().numpy()
            print("Done! Subject {}".format(i))
            # image_utilities.plot_scatter(data_dict['label'][0],data_dict['estimated'])
            # image_utilities.imagebrowse_slider(data_dict['estimated'])
            image_utilities.saveItermediateResults(data_dict, configObject.args, 'testing')
