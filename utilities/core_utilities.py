from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import logging
import numpy as np
from progress.bar import FillingSquaresBar
import torch


def str_to_class(module, classname):
    return getattr(module, classname)
    

def alternateTrainingState(args):
    while True:
        if len(args.dataloader)>1:
            yield {'training': args.dataloader[0], 'batchSize': args.batchSize}
            yield {'validation': args.dataloader[1], 'batchSize': 1}
        else:
            yield {'training': args.dataloader[0], 'batchSize': args.batchSize}


def batch_iterator(args, batchSize):
    start_index = 0
    while True:
        data = yield
        end_index = args.numberOfPatches if start_index + batchSize > args.numberOfPatches else start_index + batchSize
        batched_data = []
        for dat in data:
            batched_data.append(dat[start_index:end_index])
        yield batched_data, [end_index, start_index]
        if end_index != args.numberOfPatches:
            start_index += batchSize


def ProgressBarWrap(func):
    bar = FillingSquaresBar('', max=1, suffix='%(percent)d%%', loss=10)
    def wrapper(self, loss, args):
        bar.set_max(args.inferenceSteps)
        bar.set_total_sub(args.lenDataset)
        bar.update_ext_par(loss, args.iter)
        bar.next()
        
        if bar.index == args.inferenceSteps:
            bar.finish()
            bar.reset()
    return wrapper
    

def get_batch(data, args, batch_generator):
    data_ = []
    data_.append(data[0][0])
    args.filename = data[1][0]
    if len(data)>2:
        data_.append(data[2][0])
    if len(data)>3:
        data_.append(data[3][0])

    batched_data, indexes = batch_generator.send(data_)
    args.batchSizeIter = indexes[0] - indexes[1]

    batched_data_ = []
    batched_data_.append(batched_data[0])
    if len(batched_data)>1:
        batched_data_.append(batched_data[1])
        batched_data_.append(batched_data[2])

    return batched_data_, indexes
