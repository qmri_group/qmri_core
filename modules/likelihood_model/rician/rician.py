from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")

from qmri_core.base import base_likelihood_model
import math
import torch

class Rician(base_likelihood_model.Likelihood):
    """
        Class for the Rician PDF.
        Methods:
            - logLikelihood
                inputs: signal (measured signal), mu (simulated signal) and sigma (SD of the noise)
                outputs: data consistency loss
            - applyNoise 
                inputs: a signal and sigma (SD of the noise)
                outputs: Noisy signal
    """

    def __init__(self, configObject):
        self.__name__ = 'Rician'
        self.args = configObject.args
        super(Rician, self).__init__()

    def logLikelihood(self, signal, sigma, mu):
        pass

    def applyNoise(self, signal, sigma):
        pass

    def gradients(self, configObject, signal, kappa, sigma=1):
        pass