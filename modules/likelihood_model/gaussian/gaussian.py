from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")

from qmri_core.base import base_likelihood_model
import torch
import math
import numpy as np

class Gaussian(base_likelihood_model.Likelihood):
    """
        Class for the Gaussian PDF.
        Methods:
            - logLikelihood
                inputs: signal (measured signal), mu (simulated signal) and sigma (SD of the noise)
                outputs: data consistency loss
            - applyNoise 
                inputs: a signal and sigma
                outputs: Noisy signal corrupted by additive gaussian noise
    """
    
    def __init__(self, configObject):
        self.__name__ = 'Gaussian'
        self.args = configObject.args
        super(Gaussian, self).__init__()

    def logLikelihood(self, *args):
        signal = args[0]
        predicted_wImages = args[1]
        # sigma = torch.tensor(args[2]).type(torch.float).to(self.args.device)
        # n_elem = torch.tensor(torch.numel(args[0])).type(torch.float).to(self.args.device)
        # math_pi = torch.tensor(2*math.pi).type(torch.float).to(self.args.device)
        # return -(n_elem//2)*torch.log(math_pi) - (n_elem)*torch.log(sigma) - (1//(2*sigma**2))*torch.sum((signal - predicted_wImages)**2)
        return torch.sum((signal - predicted_wImages)**2)


    def applyNoise(self, signal, sigma):
        signal += torch.from_numpy(np.random.normal(0.0, sigma, signal.size())).to(self.args.device)
        return signal

    def gradients(self, configObject, signal, kappa, sigma=1):
        weighted_images = self.args.signal_model.generateWeightedImages(kappa)
        loss = self.logLikelihood(signal, weighted_images, sigma, self.args)
        loss.backward()
        param_map_gradient = ([param_map.grad.view(signal[0].size()) for param_map in kappa])
        return torch.stack(param_map_gradient)

    