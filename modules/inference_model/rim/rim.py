from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

from .rnn import RNN
from qmri_core.base import base_inference_method
from qmri_core.utilities.core_utilities import ProgressBarWrap
import torch
import torch.nn as nn
from torch.autograd import Variable


class Rim(base_inference_method.InferenceMethod, nn.Module):
    """
        Class Implementing the RIM model.
        Methods:
            - setOpts
                inputs: a Dict containing the key and value for a new configuration setting
            - forward
                inputs: signal (measured signal); args (options containing, at least args.batchSize and args.device)
                outputs: Estimated parameters
    """

    def __init__(self, configObject):
        super(Rim, self).__init__()
        self.__name__ = 'RIM'
        self.__require_initial_guess__ = True
        self.args = configObject.args
        self.__buildNetwork__()

    def __buildNetwork__(self):
        """
            Hidden method that instanciates a version of the RNN module based on the configuration file
        """
        self.rnn = RNN(self.args.inputChannels,
                       self.args.outputChannelsLayer1,
                       self.args.outputChannelsLayer2,
                       self.args.outputChannelsLayer3,
                       self.args.outputChannels,
                       self.args)

    def __initHidden__(self, signal):
        """
            Initialises all hidden states in the network
        """
        shape_input = torch.tensor((signal.shape)[2:])
        shape_hs1 = [1, self.args.batchSizeIter*int(torch.prod(shape_input)), self.args.outputChannelsLayer1]
        shape_hs2 = [1, self.args.batchSizeIter*int(torch.prod(shape_input)), self.args.outputChannelsLayer3]
        st_1 = Variable(torch.zeros(tuple(shape_hs1)).to(device=self.args.device))
        st_2 = Variable(torch.zeros(tuple(shape_hs2)).to(device=self.args.device))
        return [st_1, st_2]

    def __getGradients__(self, signal, maps):
        """
            Compute and return gradients of the Likelihood Function w.r.t. parameter maps
        """
        gradientParamBatch = []
        for batch in range(len(maps)):
            clonedMaps = [Variable(pmap.clone(), requires_grad=True) for pmap in maps[batch]]
            gradientParamBatch.append(self.args.likelihood_model.gradients(self.args, signal[batch], clonedMaps))
        paramGrad = torch.stack(gradientParamBatch)
        paramGrad[torch.isnan(paramGrad)] = 0
        paramGrad[torch.isinf(paramGrad)] = 0
        return paramGrad

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, inputs):
        signal = inputs[0]
        kappa = inputs[1]

        hidden = self.__initHidden__(signal)
        estimates = []
        for _ in range(self.args.inferenceSteps):
            paramGrad = self.__getGradients__(signal, kappa)
            input = torch.cat([kappa, paramGrad], 1)
            dx, hidden = self.rnn.forward(input, hidden)
            kappa = torch.abs(kappa + dx)

            if self.args.stateName == "training":
                estimates.append(kappa)
            if self.args.stateName == "testing":
                self.update_bar('-', self.args)

        if self.args.stateName == "validation" or self.args.stateName == "testing":
            estimates = kappa[0]

        return estimates
