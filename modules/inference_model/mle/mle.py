from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

from qmri_core.base import base_inference_method
from qmri_core.utilities.core_utilities import ProgressBarWrap
import torch


class Mle(base_inference_method.InferenceMethod, object):
    """
    """
    def __init__(self, configObject):
        super(Mle, self).__init__()
        self.__name__ = 'MLE'
        self.__require_initial_guess__ = True
        self.args = configObject.args

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, inputs):
        signal = inputs[0]
        initial_kappa = inputs[1]

        self.args.optimizer = torch.optim.Adam(list(initial_kappa), lr=self.args.learningRate, betas=(0.8, 0.899))
        for _ in range(self.args.inferenceSteps):
            self.args.optimizer.zero_grad()

            weighted_images = self.args.signal_model.generateWeightedImages(initial_kappa)
            loss = self.args.noise_model.logLikelihood(signal, weighted_images, self.args.sigmaNoise)
            self.update_bar(loss.item(), self.args)

            loss.backward()

            self.args.optimizer.step()
            estimates = initial_kappa

        return torch.stack(estimates)
