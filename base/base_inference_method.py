from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")
from abc import ABC, abstractmethod


class InferenceMethod(ABC):
    """Base abstract class for implementation of datasets.
        Any new inference method class should inherit from this class and implement the following abstract methods:
        - forward(): Contains the forward model of the inference method.

    Usage:
        class CustomInferenceMethod(InferenceMethod):
            def __init__(self):
                super().__init__()

            def forward():
                # Implement me
                pass
    """

    def __init__(self):
        super().__init__()

    @abstractmethod
    def forward(self):
        raise NotImplementedError("Forward method not implemented")

    



    


