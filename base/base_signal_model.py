from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from abc import ABC, abstractmethod

class SignalModel(ABC):
    """Base abstract class for implementation of signal models.
        Any new signal model class should inherit from this class and implement the following 
        abstract methods:

        - forwardModel(): Contains the forward model representing the signal generation 
        process and depends on independent parameters.
        - generateWeightedImages(): Wrapper to generate a series of independent signals using forwardModel().
        - initializeParameters(): Initialize independent parameters of the signal model.

    Usage:
        class CustomInferenceMethod(InferenceMethod):
            def __init__(self):
                super().__init__()

            def applyNoise():
                # Implement me
                pass

            def logLikelihood():
                # Implement me
                pass

            def gradients():
                # Implement me
                pass
    """
    
    def __init__(self):
        super().__init__()

    @abstractmethod
    def forwardModel(self):
        raise NotImplementedError("Forward_model not implemented")

    @abstractmethod
    def generateWeightedImages(self):
        raise NotImplementedError("Generate_weighted_sequence not implemented")

    @abstractmethod
    def initializeParameters(self):
        raise NotImplementedError("Parameter initialization method not implemented")