from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")
from abc import ABC, abstractmethod

class Likelihood(ABC):
    """Base abstract class for implementation of likelihood models.
        Any new likelihood model class should inherit from this class and implement the following abstract methods:
        - applyNoise(): Applies noise to the input data. Noise values are drawn from respective PDF.
        - logLikelihood(): Contains the log likelihood function, derived from the PDF
        - gradients(): Compute the gradient of the log likelihood function w.r.t. the its parameters.

    Usage:
        class CustomInferenceMethod(InferenceMethod):
            def __init__(self):
                super().__init__()

            def applyNoise():
                # Implement me
                pass

            def logLikelihood():
                # Implement me
                pass

            def gradients():
                # Implement me
                pass
    """

    def __init__(self):
        super().__init__()

    @abstractmethod
    def applyNoise(self):
        raise NotImplementedError("Apply Noise not implemented")

    @abstractmethod
    def logLikelihood(self):
        raise NotImplementedError("Likelihood Function not implemented")

    @abstractmethod
    def gradients(self):
        raise NotImplementedError("gradients Function not implemented")


        
