from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
from abc import ABC, abstractmethod


class Dataset(ABC):
    """Base abstract class for implementation of datasets.
        Any new dataset class should inherit from this class and implement the following abstract methods:
        - index_files(): Index all filenames in a given directory into a class property.
        - set_data_path(): Set path to data, in runtime. Required for compatibility with SampleDatabase wrapper
        - get_length(): Returns the number of files indexed.
        - get_label(): Returns the target data for training (if present).
        - get_signal(): Returns the input data for the inference method.

    Usage:
        class CustomDataset(Dataset):
            def __init__(self):
                super().__init__()

            def index_files():
                # Implement me
                pass
            
            def get_length():
                # Implement me
                pass

            def get_label():
                # Implement me
                pass

            def get_signal():
                # Implement me
                pass
    """
    
    def __init__(self):
        super().__init__()
        
    @abstractmethod
    def index_files(self):
        raise NotImplementedError("index_files of dataset not implemented")

    @abstractmethod
    def set_data_path(self):
        raise NotImplementedError("set_data_path of dataset not implemented")


    


    @abstractmethod
    def get_length(self):
        raise NotImplementedError("get_length of dataset not implemented")

    @abstractmethod
    def get_label(self):
        raise NotImplementedError("get_training_label not implemented")

    @abstractmethod
    def get_signal(self):
        raise NotImplementedError("get_training_signal not implemented")
